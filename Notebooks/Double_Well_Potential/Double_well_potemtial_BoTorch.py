#!/usr/bin/env python
# coding: utf-8


from set_seed import set_seed

from bo_steps import run_bo_kg
import torch
from botorch.utils.sampling import draw_sobol_samples

from cost_model import FlexibleFidelityCostModel
from botorch.acquisition.cost_aware import InverseCostWeightedUtility
from Multi_fidelity_1d import multi_fidelity_1d 
from sklearn.preprocessing import MinMaxScaler
import pickle
import time
import numpy as np


###################################################

def run_MA2X_Bayesian(seed,BO_initial_high,BO_initial_low):

    ################  parameters #################
    tkwargs = {
        "dtype": torch.double,
        "device": torch.device("cpu" if torch.cuda.is_available() else "cpu"),
            }

    n_high=BO_initial_high
    n_low=BO_initial_low
    bounds = torch.tensor([[-2.5, 0], [3, 1]], **tkwargs)
    target_fidelities = {1: 1.0}
    fidelities = torch.tensor([1.0, 0.46], **tkwargs)
    qual_index = {1:2}
    N_ITER=1


    #############################################
    

    set_seed(seed)
    problem = lambda x: multi_fidelity_1d(x,negate=True, mapping = None).to(**tkwargs)

    cost_model = FlexibleFidelityCostModel(values={'1.0':1000, '0.46':1.0}, fixed_cost=0)

    
    ################### Initialize Data ###############################
    def generate_initial_data(n_high,n_low):
    # generate training data
        train_x_high = draw_sobol_samples(bounds[:,:-1],n=n_high,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
        train_f_high = torch.ones((n_high,1)) * fidelities[0]
        high_full=torch.cat((train_x_high, train_f_high), dim=1)
        train_x_low = draw_sobol_samples(bounds[:,:-1],n=n_low,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
        train_f_low = torch.ones((n_low,1)) * fidelities[1]
        low_full=torch.cat((train_x_low, train_f_low), dim=1)
        train_x_full = torch.cat((high_full, low_full), dim=0)
        train_obj = problem(train_x_full).unsqueeze(-1)  # add output dimension
        return train_x_full, train_obj


    Xtrain_x, train_obj = generate_initial_data(n_high,n_low)
    train_obj = train_obj.reshape(-1,1)
    scaler = MinMaxScaler()

    scaler.fit(Xtrain_x[:,0:-1])
    Xtrain_x[:,0:-1]=torch.from_numpy(scaler.transform(Xtrain_x[:,0:-1]))
    ######################################################################
    
    # from botorch.models.cost import AffineFidelityCostModel
    # cost_model = AffineFidelityCostModel(fidelity_weights={1: 5.0}, fixed_cost=5.0)

    cost_aware_utility = InverseCostWeightedUtility(cost_model=cost_model)

    bestf, cost, best_x, final_rec, y_final,Fidelity = run_bo_kg(model_name = 'botorch', train_x = Xtrain_x, train_obj = train_obj, problem = problem, cost_model = cost_model, 
        N_ITER = N_ITER, bounds= bounds, target_fidelities= target_fidelities, cost_aware_utility = cost_aware_utility,fixed_features_list=[{1: 1.0}, {1: 0.46}], qual_index = qual_index,transform=scaler)


    return np.array(bestf), np.array(cost), np.array(best_x),np.array(final_rec),np.array(y_final), np.array(Fidelity)



if __name__ == '__main__':

    t1 = time.time()
    np.random.seed(12345)
    random_seed = np.random.choice(range(0,1000), size=15, replace=False)
    output = {'best_f':[], 'cost':[], 'Fidelity':[]}
    itr = 0
    for seed in random_seed:
        itr += 1
        [bestf, cost, best_x,final_rec,y_final,Fidelity] = \
            run_MA2X_Bayesian(seed = seed, BO_initial_high = 5,BO_initial_low=10)
        print(f'***************** Random state: {itr} *****************')
        output['best_f'].append(bestf)
        output['cost'].append(cost)
        output['Fidelity'].append(Fidelity)

    print(f'total time is {time.time() - t1}')
