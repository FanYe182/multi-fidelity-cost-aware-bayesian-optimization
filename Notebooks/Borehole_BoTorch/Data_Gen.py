# -*- coding: utf-8 -*-
"""
This module contains functions to generate data for multi-fidelity problems.
Inputs for each function are, in order, n_train, n_val, and n_test.
Data is sampled via sobol sequence.
Data generation functions should scale both inputs and outputs to [0,1] via
min-max normalization for consistency, and should return the min and max used
for both the input and output so that data can be un-scaled. If other
normalization/scaling is desired by the user, this can be achieved by un-
scaling the data and then applying the appropriate normalization as part of
data preprocessing (e.g. Z-score normalization).

Required libraries:
    numpy
    pyDOE
    tensorflow

Contains the following functions:
    SinWave_v1_MX
    Analytic_1_MF
    Borehole_MF
    MF_Data_Gen

Created on Thu May 12 17:00:06 2022

@author: Jonathan Tammer Eweis-LaBolle
"""

# Import statements
import numpy as np
from pyDOE import lhs
import tensorflow as tf
k = tf.keras
km = k.models
kl = k.layers
ko = k.optimizers
kcb = k.callbacks
kr = k.regularizers



def Borehole_MF(n_train, var = (0, 0, 0, 0)):
    # Set parameters
    dx = 9
    dnum = 8
    dy = 1
    # dt is a list whose indices correspond to the categorical variable
    # and whose entries correspond to the number of levels for that variable
    dt = (4,)
    dsource = (4,)
    num_idx = np.array(list(range(0, 8)))
    source_idx = np.array([8])
    MIN = (100, 990, 700, 100, .05, 10, 1000, 6000)
    MAX = (1000, 1110, 820, 10000, .15, 500, 2000, 12000)
    RANGE = [MAX[i] - MIN[i] for i in range(dnum)]
    # Define functions
    def y_h(x):
        Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[:, i] for i in range(8)]
        Numer = 2*np.pi*Tu
        return (Numer*(Hu-Hl))/(np.log(r/rw)*(1+((2*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
    # def y_l1(x):
    #     Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[:, i] for i in range(8)]
    #     Numer = 2*np.pi*Tu
    #     return (Numer*(Hu-.8*Hl))/(np.log(r/rw)*(1+((1*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
    def y_l1(x):
        Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[:, i] for i in range(8)]
        Numer = 2*np.pi*Tu
        return (Numer*(1.09*Hu-Hl))/(np.log(4*r/rw)*(1+((3*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
    # def y_l2(x):
    #     Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[:, i] for i in range(8)]
    #     Numer = 2*np.pi*Tu
    #     return (Numer*(Hu-Hl))/(np.log(r/rw)*(1+((8*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+.75*(Tu/Tl)))
    def y_l2(x):
        Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[:, i] for i in range(8)]
        Numer = 2*np.pi*Tu
        return (Numer*(1.05*Hu-Hl))/(np.log(2*r/rw)*(1+((3*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
    def y_l3(x):
        Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[:, i] for i in range(8)]
        Numer = 2*np.pi*Tu
        return (Numer*(1.1*Hu-Hl))/(np.log(4*r/rw)*(1+((2*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
    

    y_list = [y_h, y_l1, y_l2, y_l3]
    y = lambda x, t: y_list[t](x)
    # Preallocate data arrays and indices
    # First, get the total number of input points for each set
    train_tot = sum(n_train)
    x_train = np.empty([train_tot, dx])
    y_train = np.empty([train_tot, dy])
    # Initialize the indices for training
    train_start = 0
    fidelities=[1.0, 0.75, 0.5, 0.25]
    # Generate data
    for i in range(dt[0]):
        # Set end indices
        train_end = train_start + n_train[i]
        n_train_temp = n_train[i]
        x_train_temp = lhs(dnum, samples = n_train_temp) * RANGE + MIN
        y_train_temp = y(x_train_temp, i).reshape((n_train_temp, dy))
        y_train_temp = y_train_temp + np.sqrt(var[i])*np.random.standard_normal(size=y_train_temp.shape)
        # Store the data
        x_train[train_start : train_end, 0:dnum] = x_train_temp
        x_train[train_start : train_end, dnum:dnum+source_idx[0]] = fidelities[i]*np.ones((np.shape(x_train_temp)[0], 1))
        y_train[train_start : train_end, :] = y_train_temp
        # Update indices
        train_start = train_end

    return x_train , -1*y_train



