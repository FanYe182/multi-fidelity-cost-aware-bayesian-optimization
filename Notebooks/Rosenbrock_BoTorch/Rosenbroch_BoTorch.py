import os
import torch
from botorch.test_functions.synthetic import SyntheticTestFunction
from botorch.utils.sampling import draw_sobol_samples
from Rosenbrock import generate_rosen, multi_fidelity_rosen
from sklearn.preprocessing import MinMaxScaler
import numpy as np
import time
import pickle
from set_seed import set_seed
from botorch import fit_gpytorch_model

from botorch.acquisition.cost_aware import InverseCostWeightedUtility
from botorch.acquisition import PosteriorMean
from botorch.acquisition.knowledge_gradient import qMultiFidelityKnowledgeGradient
from botorch.acquisition.fixed_feature import FixedFeatureAcquisitionFunction
from botorch.optim.optimize import optimize_acqf
from botorch.acquisition.utils import project_to_target_fidelity
from botorch.models.gp_regression_fidelity import SingleTaskMultiFidelityGP
from botorch.models.transforms.outcome import Standardize
from gpytorch.mlls.exact_marginal_log_likelihood import ExactMarginalLogLikelihood
from cost_model import FlexibleFidelityCostModel

def run_MA2X_Bayesian(seed,BO_initial_high,BO_initial_low):
    tkwargs = {
        "dtype": torch.double,
        "device": torch.device("cuda" if torch.cuda.is_available() else "cpu"),
    }
    SMOKE_TEST = os.environ.get("SMOKE_TEST")

    ##################

    set_seed(seed)
    problem = lambda x: multi_fidelity_rosen(x,negate=True, mapping = None).to(**tkwargs)
    fidelities = torch.tensor([1.0, 0.75], **tkwargs)

    #################################

    


    def generate_initial_data(n_high,n_low):
    # generate training data
        train_x_high = draw_sobol_samples(bounds[:,:-1],n=n_high,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
        train_f_high = torch.ones((n_high,1)) * fidelities[0]
        high_full=torch.cat((train_x_high, train_f_high), dim=1)
        train_x_low = draw_sobol_samples(bounds[:,:-1],n=n_low,q = 1, batch_shape= None).squeeze(1).to(**tkwargs)
        train_f_low = torch.ones((n_low,1)) * fidelities[1]
        low_full=torch.cat((train_x_low, train_f_low), dim=1)
        train_x_full = torch.cat((high_full, low_full), dim=0)
        train_obj = problem(train_x_full).unsqueeze(-1)  # add output dimension
        return train_x_full, train_obj




    def initialize_model(train_x, train_obj):
        model = SingleTaskMultiFidelityGP(train_x, train_obj, outcome_transform=Standardize(m=1), data_fidelity=2)
        mll = ExactMarginalLogLikelihood(model.likelihood, model)
        return mll, model

    #########################

    
    bounds=torch.tensor([[-2,-2,0],[2,2,1]], **tkwargs)
    target_fidelities = {2: 1.0}

    
    cost_model = FlexibleFidelityCostModel(values={'1.0':1000, '0.75':1}, fixed_cost=0)
    cost_aware_utility = InverseCostWeightedUtility(cost_model=cost_model)

    def project(X):
        return project_to_target_fidelity(X=X, target_fidelities=target_fidelities)

    def get_mfkg(model):
        
        curr_val_acqf = FixedFeatureAcquisitionFunction(
            acq_function=PosteriorMean(model),
            d=3,
            columns=[2],
            values=[1],
        )
        
        _, current_value = optimize_acqf(
            acq_function=curr_val_acqf,
            bounds=bounds[:,:-1],
            q=1,
            num_restarts=10 if not SMOKE_TEST else 2,
            raw_samples=1024 if not SMOKE_TEST else 4,
            options={"batch_limit": 10, "maxiter": 200},
        )
            
        return qMultiFidelityKnowledgeGradient(
            model=model,
            num_fantasies=128 if not SMOKE_TEST else 2,
            current_value=current_value,
            cost_aware_utility=cost_aware_utility,
            project=project,
        )

    ################################

    from botorch.optim.optimize import optimize_acqf_mixed


    torch.set_printoptions(precision=3, sci_mode=False)

    NUM_RESTARTS = 5 if not SMOKE_TEST else 2
    RAW_SAMPLES = 128 if not SMOKE_TEST else 4
    BATCH_SIZE = 1


    def optimize_mfkg_and_get_observation(mfkg_acqf):
        """Optimizes MFKG and returns a new candidate, observation, and cost."""

        # generate new candidates
        candidates, _ = optimize_acqf_mixed(
            acq_function=mfkg_acqf,
            bounds=bounds,
            fixed_features_list=[{2: 0.75}, {2: 1.0}],
            q=BATCH_SIZE,
            num_restarts=NUM_RESTARTS,
            raw_samples=RAW_SAMPLES,
            # batch_initial_conditions=X_init,
            options={"batch_limit": 5, "maxiter": 200},
        )

        # observe new values
        cost = cost_model(candidates).sum()
        new_x = candidates.detach()
        new_obj = problem(new_x).unsqueeze(-1)
        print(f'iteration{i}')
        return new_x, new_obj, cost


    ##############################
    scaler = MinMaxScaler()
    train_x, train_obj = generate_initial_data(n_high=BO_initial_high, n_low=BO_initial_low)
    scaler.fit(train_x[:,0:-1])
    train_x[:,0:-1]=torch.from_numpy(scaler.transform(train_x[:,0:-1]))
    ###############################

    def get_recommendation(model):
        rec_acqf = FixedFeatureAcquisitionFunction(
            acq_function=PosteriorMean(model),
            d=3,
            columns=[2],
            values=[1],
        )

        final_rec, _ = optimize_acqf(
            acq_function=rec_acqf,
            bounds=bounds[:,:-1],
            q=1,
            num_restarts=10,
            raw_samples=512,
            options={"batch_limit": 5, "maxiter": 200},
        )
        
        final_rec = rec_acqf._construct_X_full(final_rec)
        
        objective_value = problem(final_rec)
      
        return final_rec, objective_value



    cumulative_cost =cost_model(train_x).sum()
    N_ITER = 60 if not SMOKE_TEST else 1
    cumulative_cost_hist=[]
    bestf=[]
    recommended_y=[]
    Fidelity=[]
    cost_max=40000
    # for i in range(N_ITER):
    i=0
    while cumulative_cost < cost_max:
        i+=1
        mll, model = initialize_model(train_x, train_obj)
        fit_gpytorch_model(mll)
        mfkg_acqf = get_mfkg(model)
        new_x, new_obj, cost = optimize_mfkg_and_get_observation(mfkg_acqf)
        new_x[:,0:-1]=torch.tensor(scaler.transform(new_x[:,0:-1]))
        train_x = torch.cat([train_x, new_x])
        train_obj = torch.cat([train_obj, new_obj])
        cumulative_cost += cost
        cumulative_cost_hist.append(cumulative_cost.clone().cpu().numpy())
        final_rec, final_y = get_recommendation(model)
        recommended_y.append(final_y)
        bestf.append(max(recommended_y))
        Fidelity.append(new_x[0][-1])
        


    ##################################


    ############################
    final_rec = get_recommendation(model)
    print(f"\ntotal cost: {cumulative_cost}\n")

    return np.array(bestf), np.array(cumulative_cost_hist),np.array(Fidelity)


if __name__ == '__main__':

    t1 = time.time()
    np.random.seed(123)
    random_seed = np.random.choice(range(0,1000), size=20, replace=False)
    output = {'best_f':[], 'cost':[], 'Fidelity':[]}
    itr = 0
    for seed in random_seed:
        itr += 1
        [bestf,cost,Fidelity] = \
            run_MA2X_Bayesian(seed = seed, BO_initial_high = 5,BO_initial_low=20)
        print(f'***************** Random state: {itr} *****************')
        output['best_f'].append(bestf)
        output['cost'].append(cost)
        output['Fidelity'].append(Fidelity)
        
        print(f'total time is {time.time() - t1}')
