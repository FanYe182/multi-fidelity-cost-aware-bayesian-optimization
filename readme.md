# Multi-Fidelity Cost-Aware Bayesian Optimization

Bayesian optimization (BO) is increasingly employed in critical applications such as materials design and drug discovery. we propose a multi-fidelity cost-aware BO framework that dramatically outperforms the state-of-the-art technologies in terms of efficiency, consistency, and robustness. We demonstrate the advantages of our framework on analytic and engineering problems and argue that these benefits stem from our two main contributions: (1) we develop a novel acquisition function for multi-fidelity cost-aware BO that safeguards the convergence against the biases of low-fidelity data, and (2) we tailor a newly developed emulator for multi-fidelity BO which enables us to not only simultaneously learn from an ensemble of multi-fidelity datasets, but also identify the severely biased low-fidelity sources that should be excluded from BO.

As schematically demonstrated in below figure, BO has two main ingredients that interact sequentially to optimize a black-box and expensive-to-evaluate objective function. These two ingredients include an emulator (i.e., a probabilistic surrogate) and an acquisition function (AF). We provide new perspectives for learning from multi-fidelity sources in the context of BO. In
particular, we argue that (a) the emulator must fuse the multi-source data in a nonlinearly learnt manifold to
maximally leverage cross-source correlations and also indicate trustworthy LF sources that do not deteriorate
BO’s performance, and (b) the AF should use the available information on the LF sources (HF source) solely
for exploration (exploitation).

![image](figures/Figure1.PNG)

In terms of finding the
true optimum (compare the blue dots to the horizontal dashed line), LMGPCA outperforms all other methods
and is followed by LMGPEI and then LMGPPI (the high accuracy of the SF methods in finding the ground
truth is expected since they only sample from the HF source and neither of the two termination criteria are
stringent). However, BoTorch either terminates at an incorrect solution or is even costlier than SF methods.
This poor performance is expected since, even though BoTorch is lookahead, its AF cannot handle the high
cost-ratios across the sources and its emulator does not effectively learn the nonlinear relations between the
HF and LF sources.

![image](figures/Figure8.PNG)


## Publications


## License

Feel free to clone this repository and modify it! If it's of good use for you, please consider giving this repository a star and citing our publications.  
If there are questions left, just contact us:  Raminb@uci.edu

## References

1. [Eweis-Labolle, J. T., Oune, N., & Bostanabad, R. (2022). Data Fusion With Latent Map Gaussian Processes. Journal of Mechanical Design, 144(9), 091703.](https://asmedigitalcollection.asme.org/mechanicaldesign/article/144/9/091703/1140790/Data-Fusion-With-Latent-Map-Gaussian-Processes)  

2. [Botorch Website](https://botorch.org/)
